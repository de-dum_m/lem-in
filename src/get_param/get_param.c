/*
** get_param.c for lem_in in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:12:59 2014 abraha_c
** Last update Fri May  2 14:13:01 2014 abraha_c
*/

#include <stdlib.h>
#include "tools.h"
#include "get_next_line.h"
#include "get_param.h"

static int	check_line(char *str)
{
  if (my_str_isnum(str) == SUCCESS && my_getnbr(str) > 0)
    return (NBR_ANT);
  if (str[0] == COMMENT_CHAR  && str[1] != COMMENT_CHAR)
    return (COMMENT);
  if (str[0] == COMMENT_CHAR  && str[1] == COMMENT_CHAR)
    return (DOUBLE_COM);
  if (is_in_str('-', str) >= SUCCESS)
    return (LINK);
  if (is_in_str(' ', str) == 2)
    return (NODES);
  return (usage("invalid argument", STDERR));
}

static	int	init_val(t_param *ptrs, int (*tab[4])(t_param *, char *, int))
{
  ptrs->start = NULL;
  ptrs->end = NULL;
  ptrs->tab = NULL;
  ptrs->nb_ants = FAILURE;
  tab[0] = &count_ant;
  tab[1] = &double_com;
  tab[2] = &create_nodes;
  tab[3] = &create_link;
  return (SUCCESS);
}

static t_param	*check_anthill(t_param *ptrs)
{
  if (seek_and_fill_start(ptrs) == FAILURE)
    return (usage_null("No start or start not linked!", ptrs, STDERR));
  if (seek_end(ptrs) == FAILURE)
    return (usage_null("No end or end not linked!", ptrs, STDERR));
  return (ptrs);
}

t_param		*get_param(int mode)
{
  t_param	*ptrs;
  char		*str;
  int		nb;
  int		(*tab[4])(t_param *, char *, int);

  if (!(ptrs = malloc(sizeof(t_param))))
    return (usage_null("Malloc failed", ptrs, STDERR));
  init_val(ptrs, tab);
  while ((str = get_next_line(0)))
    {
      if (!mode)
      	my_printf("%s\n", str);
      nb = check_line(str);
      if (nb != FAILURE && nb != 4)
	if (tab[nb](ptrs, str, mode) == FAILURE)
	  return (NULL);
      free(str);
      if (nb == FAILURE)
	return (check_anthill(ptrs));
    }
  return (check_anthill(ptrs));
}
