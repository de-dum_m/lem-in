/*
** free_tab.c for lem_in in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:12:42 2014 abraha_c
** Last update Fri May  2 14:12:45 2014 abraha_c
*/

#include <stdlib.h>
#include "get_param.h"

int		free_tab(char **tab)
{
  int		index;

  index = 0;
  while (tab && tab[index])
    free(tab[index++]);
  free(tab);
  return (SUCCESS);
}
