/*
** get_param_functions.c for get_param in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/get_param
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 17:43:18 2014
** Last update Sat May  3 18:59:11 2014 armita_a
*/

#include <stdlib.h>
#include "tools.h"
#include "get_param.h"
#include "get_next_line.h"

int		count_ant(t_param *ptrs, char *str, int mode)
{
  (void)mode;
  if ((ptrs->nb_ants = my_getnbr(str)) <= SUCCESS)
    return (usage("Invalid number of ants", STDERR));
  return (SUCCESS);
}

int		create_nodes(t_param *ptrs, char *str, int mode)
{
  char		**tab;
  int		count;
  static int	index;

  (void)mode;
  if (!(ptrs->tab = realloc(ptrs->tab, sizeof(t_graph) * (index + 1))))
    return (usage("Malloc failed", STDERR));
  if (!(ptrs->tab[index] = malloc(sizeof(t_graph))))
    return (usage("Malloc failed", STDERR));
  tab = str_to_wordtab(str, ' ');
  ptrs->tab[index]->name = my_str_copy(tab[0], 0);
  ptrs->tab[index]->flag = 0;
  ptrs->tab[index]->nb_ant = 0;
  ptrs->tab[index]->x = my_getnbr(tab[1]);
  ptrs->tab[index]->y = my_getnbr(tab[2]);
  ptrs->tab[index]->link = NULL;
  ptrs->tab[index]->waiting_list = NULL;
  ptrs->tab[++index] = NULL;
  count = 0;
  while (tab && tab[count])
    free(tab[count++]);
  free(tab);
  return (SUCCESS);
}

int		double_com(t_param *ptrs, char *str, int mode)
{
  char		*node;
  int		index;

  if (!(node =  get_next_line(0)))
    return  (usage("Invalid argument", STDERR));
  if (!mode)
    my_printf("%s\n", node);
  if (create_nodes(ptrs, node, mode) == FAILURE)
    return (FAILURE);
  index = my_strlen_ptrs_tab(ptrs->tab) - 1;
  if (my_strcmp(str, "##start") == 0)
    {
      ptrs->start = ptrs->tab[index];
      ptrs->tab[index]->flag = START;
    }
  else if (my_strcmp(str, "##end") == 0)
    {
      ptrs->end = ptrs->tab[index];
      ptrs->tab[index]->flag = END;
    }
  free(node);
  return (SUCCESS);
}

static int	check_link(int *mode, t_graph *ptr, t_graph *ptr2)
{
  int		index;

  (void)*mode;
  index = 0;
  if (ptr == ptr2)
    return (FAILURE);
  while (ptr && ptr->link && ptr->link[index])
    {
      if (ptr->link[index] == ptr2)
	return (FAILURE);
      index++;
    }
  return (SUCCESS);
}

int		create_link(t_param *ptrs, char *str, int mode)
{
  t_graph	*ptr;
  t_graph	*ptr2;
  char		**tab;
  int		index;

  tab = str_to_wordtab(str, '-');
  if (!(ptr = search_nodes(tab[0], FAILURE, ptrs->tab))
      || (!(ptr2 = search_nodes(tab[1], FAILURE, ptrs->tab))))
    return (usage("impossible link", STDERR));
  if (check_link(&mode, ptr, ptr2) == FAILURE)
    return (SUCCESS);
  index = my_strlen_ptrs_tab(ptr->link);
  if (!(ptr->link = realloc(ptr->link, sizeof(t_graph) * (index + 1))))
    return (usage("Malloc failed", STDERR));
  ptr->link[index] = ptr2;
  ptr->link[index + 1] = NULL;
  index = my_strlen_ptrs_tab(ptr2->link);
  if (!(ptr2->link = realloc(ptr2->link, sizeof(t_graph) * (index + 1))))
    return (usage("Malloc failed", STDERR));
  ptr2->link[index] = ptr;
  ptr2->link[index + 1] = NULL;
  index = 0;
  free_tab(tab);
  return (SUCCESS);
}
