/*
** seek_and_fill.c for lem_in in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:13:34 2014 abraha_c
** Last update Fri May  2 14:13:37 2014 abraha_c
*/

#include <stdlib.h>
#include "tools.h"
#include "get_param.h"

int	seek_and_fill_start(t_param *ptrs)
{
  int	i;

  i = 0;
  while (ptrs && ptrs->tab && ptrs->tab[i])
    {
      if (ptrs->tab[i]->flag == START)
        {
          ptrs->start = ptrs->tab[i];
          ptrs->tab[i]->nb_ant = ptrs->nb_ants;
          if (ptrs->start->link == NULL)
            return (FAILURE);
          return (SUCCESS);
        }
      i++;
    }
  return (FAILURE);
}

int	seek_end(t_param *ptrs)
{
  int	i;

  i = 0;
  while (ptrs->tab[i])
    {
      if (ptrs->tab[i]->flag == END)
        {
          ptrs->end = ptrs->tab[i];
          if (ptrs->end->link == NULL)
            return (FAILURE);
          return (SUCCESS);
        }
      i++;
    }
  return (FAILURE);
}
