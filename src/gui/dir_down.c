/*
** dir_down.c for gui in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Apr 30 17:44:56 2014 de-dum_m
** Last update Sun May  4 12:22:33 2014 armita_a
*/

#include <curses.h>
#include "gui.h"

static void	avoid_node_d(t_gui *gui, int mode)
{
  if (gui->pos[0] > 2 && gui->pos[0] <= gui->dest[0])
    {
      mvprintw(gui->pos[1], gui->pos[0]--, PRDCOR);
      if (mode)
	mvprintw(gui->pos[1], gui->pos[0]--, PVBAR);
      mvprintw(gui->pos[1]++, gui->pos[0], PURCOR);
      mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
    }
  else
    {
      mvprintw(gui->pos[1], gui->pos[0]++, PDRCOR);
      if (mode)
	mvprintw(gui->pos[1], gui->pos[0]++, PVBAR);
      mvprintw(gui->pos[1]++, gui->pos[0], PURCOR);
      mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
    }
}

void	link_down(t_gui *gui, chtype *s)
{
  mvinchnstr(gui->pos[1], gui->pos[0] - 1, s, 1);
  if (s[0] == NGHBAR && gui->dest[1] - gui->pos[1] > 2)
    avoid_node_d(gui, 1);
  else if ((s[0] == NGBLCOR || s[0] == NGBRCOR)
	   && gui->dest[1] - gui->pos[1] > 2)
    avoid_node_d(gui, 1);
  else if (s[0] == NPHBAR)
    mvprintw(gui->pos[1]++, gui->pos[0], PHVXBAR);
  else if (s[0] == NPVBAR)
    mvprintw(gui->pos[1]++, gui->pos[0], DPVBAR);
  else
    mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
}
