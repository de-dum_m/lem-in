/*
** dir_ddown.c for gui in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu May  1 14:30:53 2014 de-dum_m
** Last update Sat May  3 18:37:43 2014 de-dum_m
*/

#include <curses.h>
#include "gui.h"

void	link_ddown(t_gui *gui, chtype *s)
{
  mvinchnstr(gui->pos[1] + 1, gui->pos[0], s, 2);
  if ((s[1] == NGHBAR || s[1] == NGBLCOR) && gui->dest[1] - gui->pos[1] > 2)
    mvprintw(gui->pos[1]--, gui->pos[0], PVBAR);
  else if (s[0] == NGTLCOR && gui->dest[1] - gui->pos[1] > 2)
    mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
  else if (s[0] == NPDRBAR)
    mvprintw(gui->pos[1]++, gui->pos[0]++, PDXBAR);
  else
    mvprintw(gui->pos[1]++, gui->pos[0]++, PDRBAR);
}

void	link_dup(t_gui *gui, chtype *s)
{
  mvinchnstr(gui->pos[1] - 1, gui->pos[0], s, 2);
  if ((s[1] == NGHBAR || s[1] == NGBLCOR) && gui->pos[1] - gui->dest[1] > 2)
    mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
  else if (s[1] == NGTLCOR)
    mvprintw(gui->pos[1]--, gui->pos[0], PVBAR);
  else if (s[0] == NPDLBAR)
    mvprintw(gui->pos[1]--, gui->pos[0]++, PDXBAR);
  else
    mvprintw(gui->pos[1]--, gui->pos[0]++, PDLBAR);
}
