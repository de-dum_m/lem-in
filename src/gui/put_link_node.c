/*
** put_link_node.c for gui in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May  3 15:18:04 2014 de-dum_m
** Last update Sun May  4 12:10:02 2014 de-dum_m
*/

#include <stdlib.h>
#include <curses.h>
#include "gui.h"
#include "get_param.h"

static void	avoid_and_print(t_gui *gui, int dir)
{
  chtype	*s;

  if (!(s = malloc(3 * sizeof(chtype))))
    return ;
  if (dir == RIGHT)
    link_right(gui, s);
  else if (dir == UP)
    link_up(gui, s);
  else if (dir == DOWN)
    link_down(gui, s);
  else if (dir == DDOWN)
    link_ddown(gui, s);
  else if (dir == DUP)
    link_dup(gui, s);
  refresh();
}

static void	put_link(t_gui *gui)
{
  int		a;

  if ((gui->pos[0] < gui->dest[0])
      && (gui->dest[0] - gui->pos[0] > (a = ABS(gui->dest[1] - gui->pos[1]))))
    avoid_and_print(gui, RIGHT);
  else if ((gui->pos[0] < gui->dest[0] && gui->pos[1] < gui->dest[1]))
    avoid_and_print(gui, DDOWN);
  else if ((gui->pos[0] < gui->dest[0] && gui->pos[1] > gui->dest[1]))
    avoid_and_print(gui, DUP);
  else if (gui->pos[1] <= gui->dest[1])
    avoid_and_print(gui, DOWN);
  else
    avoid_and_print(gui, UP);
}

void	draw_link(t_graph *node, t_gui *gui)
{
  int	i;

  i = 0;
  while (node->link && node->link[i])
    {
      place_start(node, node->link[i], gui);
      if (gui->pos[0] <= gui->dest[0])
	while (gui->pos[0] < gui->dest[0] || gui->pos[1] != gui->dest[1])
	  put_link(gui);
      i++;
    }
}

void	draw_node(t_graph *node, t_gui *gui)
{
  int	x;
  int	y;

  x = DZR((node->x - gui->min[0]) * gui->scale[0]) + 1;
  y = DZR((node->y - gui->min[1]) * gui->scale[1]) + 1;
  mvprintw(y + 1, x, GHBAR);
  mvprintw(y - 1, x, GHBAR);
  mvprintw(y, x + 1, GVBAR);
  mvprintw(y, x - 1, GVBAR);
  mvprintw(y - 1, x + 1, GTLCOR);
  mvprintw(y - 1, x - 1, GTRCOR);
  mvprintw(y + 1, x - 1, GBLCOR);
  mvprintw(y + 1, x + 1, GBRCOR);
  mvprintw(y, x, "%s", node->name);
}
