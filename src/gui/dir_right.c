/*
** directions.c for gui in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Apr 30 14:35:37 2014 de-dum_m
** Last update Sun May  4 12:23:20 2014 armita_a
*/

#include <curses.h>
#include "gui.h"
#include "tools.h"

void	avoid_node_r(t_gui *gui, int mode)
{
  if (gui->pos[1] > 3 && gui->dest[1] <= gui->pos[1])
    {
      mvprintw(gui->pos[1]--, gui->pos[0], PRUCOR);
      if (mode)
	mvprintw(gui->pos[1]--, gui->pos[0], PVBAR);
      mvprintw(gui->pos[1]--, gui->pos[0], PVBAR);
      mvprintw(gui->pos[1], gui->pos[0]++, PDRCOR);
      mvprintw(gui->pos[1], gui->pos[0]++, PHBAR);
    }
  else
    {
      mvprintw(gui->pos[1]++, gui->pos[0], PRDCOR);
      if (mode)
	mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
      mvprintw(gui->pos[1]++, gui->pos[0], PVBAR);
      mvprintw(gui->pos[1], gui->pos[0]++, PURCOR);
      mvprintw(gui->pos[1], gui->pos[0]++, PHBAR);
    }
}

void	link_right(t_gui *gui, chtype *s)
{
  mvinchnstr(gui->pos[1], gui->pos[0], s, 3);
  if (s[2] == NGVBAR && gui->dest[0] - gui->pos[0] > 2)
    avoid_node_r(gui, 1);
  else if ((s[2] == NGBLCOR || s[2] == NGTRCOR)
	   && gui->dest[0] - gui->pos[0] > 2)
    avoid_node_r(gui, 1);
  else if (s[0] == NPVBAR)
    mvprintw(gui->pos[1], gui->pos[0]++, PHVXBAR);
  else if (s[0] == NPHBAR)
    mvprintw(gui->pos[1], gui->pos[0]++, DPHBAR);
  else
    mvprintw(gui->pos[1], gui->pos[0]++, PHBAR);
}
