/*
** place_points.c for gui in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May  3 15:24:51 2014 de-dum_m
** Last update Sun May  4 12:22:18 2014 armita_a
*/

#include "gui.h"
#include "get_param.h"

static void	put_numbers(t_gui *gui, t_graph *node,
			    t_graph *dest, int itab[4])
{
  gui->pos[0] = DZR((node->x - gui->min[0]) * gui->scale[0]) + itab[0];
  gui->pos[1] = DZR((node->y  - gui->min[1]) * gui->scale[1]) + itab[1];
  gui->dest[0] = DZR((dest->x  - gui->min[0]) * gui->scale[0]) + itab[2];
  gui->dest[1] = DZR((dest->y  - gui->min[1]) * gui->scale[1]) + itab[3];
}

void	place_start(t_graph *node, t_graph *dest, t_gui *gui)
{
  if (node->x == dest->x)
    {
      gui->pos[0] = DZR((node->x - gui->min[0]) * gui->scale[0]) + 1;
      gui->dest[0] = DZR((dest->x - gui->min[0]) * gui->scale[0]) + 1;
      if (node->y <= dest->y)
	{
	  gui->pos[1] = DZR((node->y - gui->min[1]) * gui->scale[1]) + 3;
	  gui->dest[1] = DZR((dest->y - gui->min[1]) * gui->scale[1]) - 1;
	}
      else
	{
	  gui->pos[1] = DZR((node->y - gui->min[1]) * gui->scale[1]) - 1;
	  gui->dest[1] = DZR((dest->y - gui->min[1]) * gui->scale[1]) + 1;
	}
    }
  else if (node->x < dest->x)
    put_numbers(gui, node, dest, (int[]) {3, 1, 0, 1});
  else if (node->y <= dest->y)
    put_numbers(gui, node, dest, (int[]) {1, 1, 1, 1});
}
