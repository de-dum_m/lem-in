/*
** main.c for gui in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/gui
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Wed Apr 16 08:51:38 2014
** Last update Sat May  3 18:54:04 2014 de-dum_m
*/

#include <locale.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include "get_param.h"
#include "gui.h"

static void	get_size(t_param *ptrs, int *min, int *max)
{
  int		i;

  i = 0;
  max[0] = 0;
  max[1] = 0;
  if (ptrs && ptrs->tab && ptrs->tab[i])
    {
      min[0] = ptrs->tab[i]->x;
      min[1] = ptrs->tab[i]->y;
    }
  while (ptrs && ptrs->tab && ptrs->tab[i])
    {
      if (min[0] > ptrs->tab[i]->x)
	min[0] = ptrs->tab[i]->x;
      if (min[1] > ptrs->tab[i]->y)
	min[1] = ptrs->tab[i]->y;
      if (max[0] < ptrs->tab[i]->x)
	max[0] = ptrs->tab[i]->x;
      if (max[1] < ptrs->tab[i]->y)
	max[1] = ptrs->tab[i]->y;
      i++;
    }
}

static int	rep_anthill(t_param *ptrs, t_gui *gui)
{
  int		i;

  i = 0;
  get_size(ptrs, gui->min, gui->max);
  if (!(gui->max[0] - gui->min[0]))
    gui->max[0]++;
  if (!(gui->max[1] - gui->min[1]))
    gui->max[1]++;
  gui->scale[0] = COLS / (gui->max[0] - gui->min[0]);
  gui->scale[1] = LINES / (gui->max[1] - gui->min[1]);
  if (gui->scale[0] <= 0 || gui->scale[1] <= 0)
    mvprintw(LINES / 2, COLS / 2 - 10, "Map too big to represent");
  else
    {
      if (!(gui->scale[0] /= 2))
	gui->scale[0]++;
      if (!(gui->scale[1] /= 2))
	gui->scale[1]++;
      while (ptrs && ptrs->tab && ptrs->tab[i])
	draw_node(ptrs->tab[i++], gui);
      i = 0;
      while (ptrs && ptrs->tab && ptrs->tab[i])
	draw_link(ptrs->tab[i++], gui);
    }
  return (SUCCESS);
}

static void	put_and_wait(t_gui *gui, t_param *ptrs)
{
  int		key;

  rep_anthill(ptrs, gui);
  refresh();
  keypad(stdscr, TRUE);
  while ((key = getch()) > 0)
    {
      if ((key == 27 || key == ' '))
	return ;
    }
}

int		init_gui(t_param *ptrs)
{
  FILE		*fp;
  t_gui		gui;

  setlocale(LC_ALL, "");
  if (!(fp = freopen("/dev/tty", "rw", stdin))
      || !(initscr()) || start_color() == ERR)
    return (FAILURE);
  cbreak();
  noecho();
  use_default_colors();
  put_and_wait(&gui, ptrs);
  endwin();
  fclose(fp);
  return (SUCCESS);
}
