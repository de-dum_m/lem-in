/*
** get_shortest_path.c for lem_in in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:14:18 2014 abraha_c
** Last update Fri May  2 15:57:51 2014 auduri_a
*/

#include <stdlib.h>
#include "tools.h"

t_path		*init_path(t_game *game)
{
  t_path	*new;

  if (!(new = malloc(sizeof(t_path))))
    return (NULL);
  if (game)
    new->node = game->start;
  else
    new->node = NULL;
  new->next = NULL;
  new->prev = NULL;
  new->ant = 0;
  new->wait = 0;
  new->tunnel = 0;
  return (new);
}

static t_path	*copy_path(t_path *path)
{
  t_path	*tmp;
  t_path	*tmp_prev;
  t_path	*new;
  t_path	*tmp_new;

  tmp = path;
  tmp_new = init_path(NULL);
  new = tmp_new;
  while (tmp)
    {
      new->node = tmp->node;
      new->tunnel = tmp->tunnel;
      new->wait = tmp->wait;
      new->ant = tmp->ant;
      if (if_end(tmp, new) == SUCCESS)
	return (tmp_new);
      new->next = init_path(NULL);
      tmp_prev = new;
      new = new->next;
      new->prev = tmp_prev;
      tmp = tmp->next;
    }
  new = NULL;
  return (tmp_new);
}

static t_path	*back_up(t_path *tmp, int *t, int *len)
{
  tmp = tmp->prev;
  (*t)--;
  *len -= (tmp->wait + 1);
  tmp->wait = 0;
  tmp->tunnel++;
  free(tmp->next);
  tmp->next = NULL;
  return (tmp);
}

static t_path	*go_down(t_path *tmp, int *t, int *len)
{
  tmp->next = init_path(NULL);
  tmp->next->prev = tmp;
  tmp->next->node = tmp->node->link[tmp->tunnel];
  tmp->next->ant = tmp->ant;
  *len += ((tmp->wait = get_wait(tmp->next->node, ++(*t))) + 1);
  tmp = tmp->next;
  return (tmp);
}

int		get_shortest_path(t_game *game, t_path *path, int i, int t)
{
  t_path	*ph;
  int		len;
  int		m_len;
  static int	ant_nb;

  len = 0;
  m_len = -1;
  ph = path;
  ph->ant = ++ant_nb;
  while (ph)
    {
      if (ph->node->flag == END && (m_len < 0 || len < m_len) && (m_len = len))
	  game->paths[i] = copy_path(path);
      else if (((len >= m_len && m_len > 0)
		|| (!ph->node->link || !ph->node->link[ph->tunnel])) && len > 0)
	ph = back_up(ph, &t, &len);
      else if (ph->node && ph->node->link && ph->node->link[ph->tunnel]
	       && is_in_path(path, ph->node->link[ph->tunnel]) == SUCCESS)
	ph = go_down(ph, &t, &len);
      else if (!ph->next && (!ph->node->link || !ph->node->link[ph->tunnel]))
	  return (SUCCESS);
      else
	ph->tunnel++;
    }
  return (FAILURE);
}
