/*
** check.c for resolve in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Apr 18 15:50:26 2014 de-dum_m
** Last update Tue Apr 22 20:10:43 2014 de-dum_m
*/

#include "tools.h"

int		is_in_path(t_path *path, t_graph *node)
{
  t_path	*tmp;

  tmp = path;
  while (tmp)
    {
      if (tmp->node == node)
	return (FAILURE);
      tmp = tmp->next;
    }
  return (SUCCESS);
}
