/*
** waiting_list.c for lem_in in /home/abraha_c/cpe_2013_lem-in
**
** Made by abraha_c
** Login   <abraha_c@epitech.net>
**
** Started on  Thu Apr 17 17:46:13 2014 de-dum_m
** Last update Sat May  3 19:22:21 2014 armita_a
*/

#include <stdlib.h>
#include "tools.h"

static int	int_tab_len(int *tab)
{
  int		len;

  len = 0;
  while (tab && tab[len] > 0)
    len++;
  return (len);
}

static int	*add_wait(int *tab, int t)
{
  int		i;
  int		*new;

  i = 0;
  if (!(new = mallsetint((int_tab_len(tab) + 3))))
    return (NULL);
  while (tab && tab[i] > 0 && tab[i] < t)
    {
      new[i] = tab[i];
      i++;
    }
  new[i++] = t;
  while (tab && tab[i - 1] > 0)
    {
      new[i] = tab[i - 1];
      i++;
    }
  new[i] = -1;
  return (new);
}

int		set_wait(t_path *path, int t)
{
  t_path	*tmp;

  tmp = path;
  while (tmp)
    {
      if (!tmp->node)
	return (FAILURE);
      if (tmp->node->flag == 0 &&
	  !(tmp->node->waiting_list = add_wait(tmp->node->waiting_list, t)))
	return (FAILURE);
      tmp = tmp->next;
      t++;
    }
  return (SUCCESS);
}

int		get_wait(t_graph *node, int t)
{
  int		i;
  int		wait;

  i = 0;
  wait = t;
  if (node->flag > 0)
    return (0);
  while (node->waiting_list && node->waiting_list[i] > 0
	 && node->waiting_list[i] < t)
    i++;
  if (node->waiting_list && node->waiting_list[i] == t)
    while (node->waiting_list && node->waiting_list[i] > 0
	   && node->waiting_list[++i] != ++t);
  return (t - wait);
}
