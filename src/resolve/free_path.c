/*
** free_path.c for lem_in in /home/abraha_c/cpe_2013_lem-in
**
** Made by abraha_c
** Login   <abraha_c@epitech.net>
**
** Started on  Fri May  2 14:14:01 2014 abraha_c
** Last update Sat May  3 19:08:50 2014 armita_a
*/

#include <stdlib.h>
#include "tools.h"

void	free_struct_path(t_path *path)
{
  free(path->node);
  free(path);
}
