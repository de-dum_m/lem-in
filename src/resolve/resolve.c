/*
** resolve.c for lem_in in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:14:41 2014 abraha_c
** Last update Fri May  2 14:14:42 2014 abraha_c
*/

#include <stdlib.h>
#include "tools.h"

int		if_end(t_path *tmp, t_path *new)
{
  if (tmp->node->flag == END)
    {
      if (new != NULL)
        new = NULL;
      return (SUCCESS);
    }
  return (FAILURE);
}

int		resolve(t_game *game)
{
  int		i;
  int		t;
  t_path	*path;

  i = 0;
  t = 1;
  game->paths = NULL;
  while (game->start->nb_ant > 0)
    {
      while ((game->paths = realloc(game->paths, sizeof(t_path *) * (i + 2)))
             && (path = init_path(game)) && get_shortest_path(game, path, i, t)
             && set_wait(game->paths[i++], t) && --game->start->nb_ant > 0)
        {
          if (!game->paths || !game->paths[i - 1])
            return (usage("Sorry mate, there's just no way out", 2));
          game->paths[i] = NULL;
          if (i > 0 && game->paths[i - 1]->wait > 0)
            move_ants(game, &i);
        }
      t++;
      move_ants(game, &i);
    }
  while (move_ants(game, &i) == SUCCESS);
  return (SUCCESS);
}
