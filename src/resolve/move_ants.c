/*
** move_ants.c for resolve in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Apr 18 10:29:50 2014 de-dum_m
** Last update Fri May  2 10:36:45 2014 de-dum_m
*/

#include <stdlib.h>
#include "tools.h"

static void	print_move(t_game *game, int *done, int mode)
{
  if (game->gui == SUCCESS)
    return ;
  if (mode == 1 && (*done & 2) == 2)
    {
      *done ^= 2;
      my_putchar(' ', 1);
    }
  else if (mode == 2 && (*done & 8) == 8)
    {
      *done = 0;
      my_putchar('\n', 1);
    }
}

static int	to_node(t_game *game, int j, int *done, int mode)
{
  if ((game->paths[j]->node->flag != END)
      && (game->paths[j]->next->node->nb_ant <= 0
	  || game->paths[j]->next->node->flag == END))
    {
      print_move(game, done, mode);
      if (game->paths[j]->node->flag != START)
	game->paths[j]->node->nb_ant--;
      game->paths[j] = game->paths[j]->next;
      game->paths[j]->node->nb_ant++;
      my_printf("P%d-%s", game->paths[j]->ant, game->paths[j]->node->name);
      return (SUCCESS);
    }
  else if (game->paths[j]->wait > 0 && game->paths[j]->node->flag != END)
    {
      game->paths[j]->wait--;
      return (0);
    }
  return (FAILURE);
}

int	move_ants(t_game *game, int *i)
{
  int	j;
  int	ret;
  int	done;
  int	inull;

  j = 0;
  done = 0;
  inull = 0;
  while (j < *i)
    {
      if (game->paths[j])
	{
	  if ((ret = to_node(game, j, &done, 1)) == SUCCESS)
	    done |= 10;
	  else if (ret == FAILURE)
	    inull++;
	}
      else
	inull++;
      j++;
    }
  print_move(game, &done, 2);
  if (inull == *i)
    return (FAILURE);
  return (SUCCESS);
}
