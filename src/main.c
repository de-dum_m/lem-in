/*
** main.c for src in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:49:44 2014
** Last update Sat May  3 19:08:47 2014 armita_a
*/

#include <stdlib.h>
#include "get_param.h"
#include "tools.h"
#include "gui.h"

static void	my_free_t_param(t_param *ptrs)
{
  int		index;

  index = 0;
  while (ptrs && ptrs->tab && ptrs->tab[index])
    {
      free(ptrs->tab[index]->link);
      free(ptrs->tab[index]->name);
      free(ptrs->tab[index]);
      index++;
    }
  free(ptrs->tab);
  free(ptrs);
  }

static int	get_options(char **av, int ac, t_game *game)
{
  game->gui = FAILURE;
  if (ac > 1)
    {
      if (my_strcmp(av[1], "-g") == 0 || my_strcmp(av[1], "--gui") == 0)
	game->gui = SUCCESS;
      else
	return (my_print_error("%s < map [-g, --gui]\n", av[0]));
    }
  return (SUCCESS);
}

int		main(int ac, char **av)
{
  t_param	*ptrs;
  t_game	game;

  if (!(get_options(av, ac, &game)))
    return (FAILURE);
  if (game.gui == FAILURE && !(ptrs = get_param(0)))
    return (FAILURE);
  else if (game.gui == SUCCESS && !(ptrs = get_param(1)))
    return (FAILURE);
  game.start = ptrs->start;
  game.min_len = -1;
  if (game.gui == SUCCESS)
    init_gui(ptrs);
  if (resolve(&game) == FAILURE)
    {
      if (game.gui == SUCCESS)
	endwin();
      return (FAILURE);
    }
  my_free_t_param(ptrs);
  return (SUCCESS);
}
