/*
** my_str_isnum.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:33:51 2014
** Last update Fri May  2 14:18:23 2014 abraha_c
*/

#include "tools.h"

int	my_str_isnum(char *str)
{
  int	i;
  int	compt;

  compt = 0;
  i = 0;
  while (str[i])
    {
      if ((str[i] >= '0' && str[i] <= '9') || str[i] == '-')
	compt++;
      i++;
    }
  if (i == compt)
    return (SUCCESS);
  return (FAILURE);
}
