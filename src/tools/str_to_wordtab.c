/*
** str_to_wordtab.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:33:35 2014
** Last update Fri May  2 14:21:06 2014 abraha_c
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "tools.h"

static int	compt_word(char *str, char c)
{
  int		nb_word;
  int		i;

  if (str == NULL)
    return (0);
  i = 0;
  nb_word = 1;
  while (str[i])
    {
      if (str[i] == c)
	nb_word++;
      i++;
    }
  return (nb_word);
}

static int	cpy(char *res, char *str, int i, int k)
{
  int		c;

  c = 0;
  while (i < k)
    {
      res[c] = str[i];
      c++;
      i++;
    }
  res[c] = '\0';
  return (k + 1);
}

char		**str_to_wordtab(char *str, char c1)
{
  int		i;
  int		k;
  int		c;
  char		**tab;

  i = 0;
  k = 0;
  c = 0;
  if ((tab = malloc(sizeof(char *) * (compt_word(str, c1) + 1))) == NULL)
    return (NULL);
  while (str[k])
    {
      if (str[k] == c1)
        {
	  if ((tab[c] = malloc(sizeof(char) * (k - i) + 1)) == NULL)
	    return (NULL);
          i = cpy(tab[c++], str, i, k++);
        }
      k = k + 1;
    }
  if ((tab[c] = malloc(sizeof(char) * (k - i) + 1)) == NULL)
    return (NULL);
  cpy(tab[c++], str, i, k);
  tab[c] = NULL;
  return (tab);
}
