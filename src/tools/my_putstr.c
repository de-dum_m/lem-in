/*
** my_putstr.c for tools in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:17:13 2014 abraha_c
** Last update Fri May  2 14:17:18 2014 abraha_c
*/

#include <unistd.h>
#include "tools.h"

void	my_putstr(char *str, int nb)
{
  int	index;

  index = 0;
  while (str[index])
    my_putchar(str[index++], nb);
  return ;
}
