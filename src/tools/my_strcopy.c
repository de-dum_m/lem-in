/*
** my_strcopy.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:38:16 2014
** Last update Mon Apr 14 16:38:38 2014 
*/

#include <stdlib.h>
#include "tools.h"

char	*my_str_copy(char *s, int len)
{
  int	i;
  char	*res;

  i = 0;
  if (len == 0)
    len = my_strlen(s);
  else if (len < 0)
    len = my_strlen(s) - len;
  if (!(res = malloc(len + 3)))
    return (NULL);
  while (s && s[i] && i < len)
    {
      res[i] = s[i];
      i++;
    }
  res[i] = '\0';
  return (res);
}
