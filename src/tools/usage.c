/*
** usage.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:38:57 2014
** Last update Tue Apr 15 09:15:38 2014 
*/

#include <unistd.h>
#include "tools.h"

int	usage(char *str, int fd)
{
  my_putstr("lem_in : ", fd);
  my_putstr(str, fd);
  my_putchar('\n', fd);
  if (fd == STDERR)
    return (FAILURE);
  return (SUCCESS);
}

void	*usage_null(char *str, void *ptr, int fd)
{
  my_putstr("lem_in : ", fd);
  my_putstr(str, fd);
  my_putchar('\n', fd);
  if (fd == STDERR)
    return (NULL);
  return (ptr);
}
