/*
** my_memset.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Apr 15 01:49:45 2014
** Last update Fri Apr 18 17:41:21 2014 de-dum_m
*/

#include <stdlib.h>

char	*my_memset(char *str, int len)
{
  int	i;

  i = 0;
  while (i < len)
      str[i++] = '\0';
  return (str);
}

int	*mallsetint(int size)
{
  int	i;
  int	*res;

  i = 0;
  if (!(res = malloc(sizeof(int) * size)))
    return (NULL);
  while (i < size)
    {
      res[i] = -1;
      i++;
    }
  return (res);
}
