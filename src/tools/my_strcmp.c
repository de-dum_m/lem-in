/*
** my_strcmp.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:34:00 2014
** Last update Fri May  2 14:17:57 2014 abraha_c
*/

int	my_strcmp(char *s1, char *s2)
{
  int	index;

  index = 0;
  while (s1[index] == s2[index] && s1[index] != '\0' && s2[index] != '\0')
    index = index + 1;
  if (s1 && s2 && s1[index] == s2[index])
    return (0);
  if (s1[index] == '\0' || s1[index] < s2[index])
    return (-1);
  return (1);
}
