/*
** my_putchar.c for tools in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:16:38 2014 abraha_c
** Last update Fri May  2 14:16:50 2014 abraha_c
*/

#include <unistd.h>

void	my_putchar(char c, int nb)
{
  write(nb, &c, 1);
}
