/*
** my_put_unsigned_nbr.c for minitalk in /home/de-dum_m/rendu/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar 23 16:01:49 2014 de-dum_m
** Last update Sun Mar 23 18:23:25 2014 de-dum_m
*/

#include "my_printf.h"

void	print_modified_count(char nb, int fd)
{
  my_count_putchar(nb + 48, fd);
}

int	my_put_unsigned_nbr(unsigned int nb, int fd)
{
  if (nb >= 10)
    {
      my_count_put_nbr(nb / 10, fd);
      my_count_put_nbr(nb % 10, fd);
    }
  else
    print_modified_count(nb, fd);
  return (0);
}
