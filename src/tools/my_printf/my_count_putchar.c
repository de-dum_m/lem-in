/*
** my_count_putchar.c for minitalk in /home/de-dum_m/rendu/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar 23 16:02:19 2014 de-dum_m
** Last update Fri May  2 14:44:27 2014 abraha_c
*/

#include <unistd.h>

int		count_prints()
{
  static int	prints;

  prints = prints + 1;
  return (prints);
}

void		my_count_putchar(char c, int fd)
{
  if (!c)
    return ;
  else
    write(fd, &c, 1);
  count_prints();
}

void		my_count_putstr(char *str, int fd)
{
  int		i;

  i = 0;
  while (str && str[i])
    {
      my_count_putchar(str[i], fd);
      i = i + 1;
    }
}
