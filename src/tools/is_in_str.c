/*
** is_in_str.c for lem_in in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:16:11 2014 abraha_c
** Last update Fri May  2 14:16:12 2014 abraha_c
*/

int	is_in_str(char c, char *str)
{
  int	index;
  int	nb;

  nb = 0;
  index = 0;
  while (str && str[index])
    {
      if (str[index] == c)
	nb++;
      index++;
    }
  return (nb);
}
