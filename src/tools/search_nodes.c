/*
** search_nodes.c for get_param in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/get_param
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Apr 15 09:35:11 2014
** Last update Fri May  2 14:19:33 2014 abraha_c
** Last update Tue Apr 15 14:30:41 2014 alexis abra
*/

#include <stdlib.h>
#include "tools.h"

static t_graph	*search_from_name(char *name, t_graph **tab)
{
  int		i;

  i = 0;
  while (tab[i])
    {
      if (my_strcmp(tab[i]->name, name) == 0)
	return (tab[i]);
      i++;
    }
  return (NULL);
}

static t_graph	*search_from_flag(int flag, t_graph **tab)
{
  int		i;

  i = 0;
  while (tab[i])
    {
      if (tab[i]->flag == flag)
	return (tab[i]);
      i++;
    }
  return (NULL);
}

t_graph		*search_nodes(char *name, int flag, t_graph **tab)
{
  if (name == NULL)
    return (search_from_flag(flag, tab));
  else if (flag == FAILURE)
    return (search_from_name(name, tab));
  return (NULL);
}
