/*
** my_strlen.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:38:51 2014
** Last update Tue Apr 15 13:49:21 2014 
*/

#include "tools.h"

int	my_strlen(char *str)
{
  int	index;

  index = 0;
  while (str && str[index++]);
  return (index -1);
}

int	my_strlen_ptrs_tab(t_graph **tab)
{
  int	index;

  index = 0;
  while (tab && tab[index])
    index++;
  return (index);
}
