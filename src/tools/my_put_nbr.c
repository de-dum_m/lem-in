/*
** my_put_nbr.c for tools in /home/abraha_c/cpe_2013_lem-in
** 
** Made by abraha_c
** Login   <abraha_c@epitech.net>
** 
** Started on  Fri May  2 14:17:01 2014 abraha_c
** Last update Fri May  2 14:17:02 2014 abraha_c
*/

#include "tools.h"

int	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_putchar('-', 1);
    }
  if (nb >= 10)
    {
      my_put_nbr(nb / 10);
      my_put_nbr(nb % 10);
    }
  else
    my_putchar(nb + 48, 1);
  return (0);
}
