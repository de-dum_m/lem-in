/*
** my_getnbr.c for tools in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/src/tools
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:35:06 2014
** Last update Mon Apr 14 16:36:07 2014 
*/

#include "tools.h"

int	my_getnbr(char *str)
{
  int	index;
  int	nb;
  int	is_neg;

  nb = 0;
  index = 0;
  if (!str || !str[0])
    return (FAILURE);
  while (str && (str[index] == '-' || str[index] == '+'))
    {
      nb++;
      index++;
    }
  if (nb % 2 == 0)
    is_neg = 1;
  else
    is_neg = -1;
  nb = 0;
  while (str && str[index] >= '0' && str[index] <= '9')
    nb = (nb * 10) + str[index++] - 48;
  if (str && str[index])
    return (FAILURE);
  return (is_neg * nb);
}
