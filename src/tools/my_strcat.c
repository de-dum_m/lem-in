/*
** my_strcat.c for tools in /home/abraha_c/cpe_2013_lem-in
**
** Made by abraha_c
** Login   <abraha_c@epitech.net>
**
** Started on  Fri May  2 14:17:31 2014 abraha_c
** Last update Sat May  3 19:24:45 2014 armita_a
*/

int	count_char(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    i = i + 1;
  return (i);
}

char	*my_strcat(char *dest, char *src)
{
  int	n;
  int	b;

  b = 0;
  n = count_char(dest);
  while (src[b] != '\0')
    {
      dest[n] = src[b];
      n = n + 1;
      b = b + 1;
    }
  dest[n] = '\0';
  return (dest);
}
