/*
** tools.h for includes in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/includes
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:36:43 2014
** Last update Fri May  2 10:53:25 2014 Tosiev
*/

#ifndef TOOLS_H_
# define TOOLS_H_

# define SUCCESS	1
# define FAILURE	-1

# define STDERR		2

# define START		1
# define END		2

typedef struct          s_graph
{
  struct s_graph	**link;
  char			*name;
  int			*waiting_list;
  int			x;
  int			y;
  int			nb_ant;
  int			flag;
}			t_graph;

typedef struct		s_path
{
  t_graph		*node;
  int			tunnel;
  int			wait;
  int			ant;
  struct s_path		*prev;
  struct s_path		*next;
}			t_path;

typedef struct		s_game
{
  t_graph		*start;
  t_path		**paths;
  int			min_len;
  int			gui;
}			t_game;

/*
** s_graph:
** link = tunnels
** waiting_list = list chainée des moments d'occupation du node0
** flag = 0: rien, 1: end, -1: start
*/

/*
** is_in_str.c
*/
int			is_in_str(char c, char *str);

/*
** my_getnbr.c
*/
int			my_getnbr(char *str);

/*
** my_memset.c
*/
char			*my_memset(char *str, int len);
int			*mallsetint(int size);

/*
** my_printf/my_count_putchar.c
*/
int			count_prints();
void			my_count_putchar(char c, int fd);
void			my_count_putstr(char *str, int fd);

/*
** my_printf/my_count_put_nbr_base.c
*/
int			my_count_putnbr_base(unsigned int nbr, char *base, int fd);
void			chop_chop_count(unsigned int nbr, int b, char *base, int fd);

/*
** my_printf/my_count_put_nbr.c
*/
int			my_count_put_nbr(int nb, int fd);

/*
** my_printf/my_printf.c
*/
char			*my_print_error_chret(const char *str, ...);
int			my_print_error(const char *str, ...);
int			my_printf(const char *str, ...);

/*
** my_printf/my_printf_tools.c
*/
void			my_put_hex(unsigned int ix, char type, int fd);
void			my_put_ptr(unsigned int ptr, int fd);
void			my_putstr_weird(char *str, int fd);

/*
** my_printf/my_put_unsigned_nbr.c
*/
int			my_put_unsigned_nbr(unsigned int nb, int fd);
void			print_modified_count(char nb, int fd);

/*
** my_putchar.c
*/
void			my_putchar(char c, int nb);

/*
** my_put_nbr.c
*/
int			my_put_nbr(int nb);

/*
** my_putstr.c
*/
void			my_putstr(char *str, int nb);

/*
** my_strcat.c
*/
char			*my_strcat(char *dest, char *src);
int			count_char(char *str);

/*
** my_strcmp.c
*/
int			my_strcmp(char *s1, char *s2);

/*
** my_strcopy.c
*/
char			*my_str_copy(char *s, int len);

/*
** my_str_isnum.c
*/
int			my_str_isnum(char *str);

/*
** my_strlen.c
*/
int			my_strlen(char *str);
int			my_strlen_ptrs_tab(t_graph **tab);

/*
** str_to_wordtab.c
*/
char			**str_to_wordtab(char *str, char c1);

/*
** usage.c
*/
int			usage(char *str, int fd);
void			*usage_null(char *str, void *ptr, int fd);

/*
** search_nodes.c
*/
t_graph			*search_nodes(char *name, int flag, t_graph **tab);

/*
** check.c
*/
int			is_in_path(t_path *path, t_graph *node);

/*
** get_shortest_path.c
*/
int			resolve(t_game *game);

/*
** move_ants.c
*/
int			move_ants(t_game *game, int *i);

/*
** test_map.c
*/
int			example_graph(t_graph *test);
t_graph			*create_node();

/*
** waiting_list.c
*/
int			get_wait(t_graph *node, int t);
int			set_wait(t_path *path, int t);

/*
** resolve.c
*/
int			resolve(t_game *game);
int			if_end(t_path *tmp, t_path *new);

/*
** get_shortest_path.c
*/
int			get_shortest_path(t_game *game, t_path *path, int i, int t);
t_path			*init_path(t_game *game);

/*
** free_path.c
*/
void			free_struct_game(t_game *game);

#endif /* !TOOLS_H_ */
