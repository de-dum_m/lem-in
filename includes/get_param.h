/*
** get_param.h for includes in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/includes
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:52:18 2014
** Last update Fri May  2 13:57:56 2014 Tosiev
*/

#ifndef GET_PARAM_H_
# define GET_PARAM_H_

# define COMMENT_CHAR	'#'

# define NBR_ANT	0
# define DOUBLE_COM	1
# define NODES		2
# define LINK		3
# define COMMENT	4

# include "tools.h"

typedef struct		s_param
{
  t_graph		*start;
  t_graph		*end;
  t_graph		**tab;
  int			nb_ants;
}			t_param;

/*
** get_param.c
*/
t_param			*get_param(int mode);

/*
** get_param_functions.c
*/
int			count_ant(t_param *ptrs, char *str, int mode);
int			create_link(t_param *ptrs, char *str, int mode);
int			create_nodes(t_param *ptrs, char *str, int mode);
int			double_com(t_param *ptrs, char *str, int mode);

/*
** seek_and_fill.c
*/
int			seek_and_fill_start(t_param *ptrs);
int			seek_end(t_param *ptrs);

/*
** free_tab.c
*/
int			free_tab(char **tab);

#endif /* !GET_PARAM_H_ */
