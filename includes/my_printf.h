/*
** my_printf.h for includes in /home/armita_a/Documents/Teck_1/Prog_elem/cpe_2013_lem-in/includes
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon Apr 14 16:32:48 2014
** Last update Mon Apr 14 16:32:49 2014 
*/

#ifndef MY_PRINTF_H_
# define MY_PRINTF_H_

/*
** my_count_putchar.c
*/
int	count_prints();
void	my_count_putchar(char c, int fd);
void	my_count_putstr(char *str, int fd);

/*
** my_count_put_nbr_base.c
*/
int	my_count_putnbr_base(unsigned int nbr, char *base, int fd);
void	chop_chop_count(unsigned int nbr, int b, char *base, int fd);

/*
** my_count_put_nbr.c
*/
int	my_count_put_nbr(int nb, int fd);

/*
** my_printf.c
*/
int	my_print_error(const char *str, ...);
int	my_printf(const char *str, ...);
char	*my_print_error_chret(const char *str, ...);

/*
** my_printf_tools.c
*/
void	my_put_hex(unsigned int ix, char type, int fd);
void	my_put_ptr(unsigned int ptr, int fd);
void	my_putstr_weird(char *str, int fd);

/*
** my_put_unsigned_nbr.c
*/
int	my_put_unsigned_nbr(unsigned int nb, int fd);
void	print_modified_count(char nb, int fd);

#endif /* !MY_PRINTF_H_ */
