/*
** gui.h for includes in /home/de-dum_m/code/B2-C-Prog_Elem/cpe_2013_lem-in
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Apr 26 18:52:34 2014 de-dum_m
** Last update Sat May  3 19:09:58 2014 armita_a
*/

#ifndef GUI_H_
# define GUI_H_

# include <curses.h>
# include "get_param.h"

# define ABS(x)	((x) < 0) ? ((x) * -1) : (x)
# define DZR(x)	((x) < 0) ? (1) : (x)

# define GVBAR		"┃"
# define GHBAR		"━"
# define GTLCOR		"┓"
# define GTRCOR		"┏"
# define GBLCOR		"┗"
# define GBRCOR		"┛"

# define NGVBAR		9475
# define NGHBAR		9473
# define NGTRCOR	9487
# define NGTLCOR	9491
# define NGBLCOR	9495
# define NGBRCOR	9499

# define PVBAR		"│"
# define PHBAR		"─"
# define PDRBAR		"╲"
# define PDLBAR		"╱"
# define PDXBAR		"╳"
# define PHVXBAR	"┼"
# define DPVBAR		"║"
# define DPHBAR		"═"

# define NPVBAR		9474
# define NPHBAR		9472
# define NPDRBAR	10065
# define NPDLBAR	10066

# define PRDCOR		"╮"
# define PRUCOR		"╯"
# define PDRCOR		"╭"
# define PURCOR		"╰"

# define UP		1
# define DOWN		2
# define RIGHT		3
# define DUP		4
# define DDOWN		5

typedef struct	s_gui
{
  int		min[2];
  int		max[2];
  int		scale[2];
  int		pos[2];
  int		dest[2];
  WINDOW	*bot_b;
}		t_gui;

/*
** dir_diag.c
*/
void	link_ddown(t_gui *gui, chtype *s);
void	link_dup(t_gui *gui, chtype *s);

/*
** dir_down.c
*/
void	link_down(t_gui *gui, chtype *s);

/*
** dir_right.c
*/
void	avoid_node_r(t_gui *gui, int mode);
void	link_right(t_gui *gui, chtype *s);

/*
** dir_up.c
*/
void	link_up(t_gui *gui, chtype *s);

/*
** init_gui.c
*/
int	init_gui(t_param *ptrs);

/*
** place_points.c
*/
void	place_start(t_graph *node, t_graph *dest, t_gui *gui);

/*
** put_link_node.c
*/
void	draw_link(t_graph *node, t_gui *gui);
void	draw_node(t_graph *node, t_gui *gui);

#endif /* !GUI_H_ */
