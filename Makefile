##
## Makefile for lem_in in /home/abraha_c/cpe_2013_lem-in
## 
## Made by abraha_c
## Login   <abraha_c@epitech.net>
## 
## Started on  Mon Apr 14 16:28:43 2014 
## Last update Sat May  3 19:32:06 2014 armita_a
##

NAME	= lem_in

SRC	= src/main.c					\
	src/gnl/get_next_line.c				\
	src/tools/search_nodes.c			\
	src/tools/is_in_str.c				\
	src/tools/my_getnbr.c				\
	src/tools/my_putchar.c				\
	src/tools/my_put_nbr.c				\
	src/tools/my_putstr.c				\
	src/tools/my_strcat.c				\
	src/tools/my_strcmp.c				\
	src/tools/my_strcopy.c				\
	src/tools/my_str_isnum.c			\
	src/tools/my_strlen.c				\
	src/tools/str_to_wordtab.c			\
	src/tools/usage.c				\
	src/tools/my_memset.c				\
	src/tools/my_printf/my_count_putchar.c		\
	src/tools/my_printf/my_count_put_nbr_base.c	\
	src/tools/my_printf/my_count_put_nbr.c		\
	src/tools/my_printf/my_printf.c			\
	src/tools/my_printf/my_printf_tools.c		\
	src/tools/my_printf/my_put_unsigned_nbr.c	\
	src/gui/init_gui.c				\
	src/gui/dir_up.c				\
	src/gui/dir_right.c				\
	src/gui/dir_down.c				\
	src/gui/dir_diag.c				\
	src/gui/put_link_node.c				\
	src/gui/place_points.c				\
	src/get_param/get_param.c			\
	src/get_param/seek_and_fill.c			\
	src/get_param/get_param_functions.c		\
	src/get_param/free_tab.c			\
	src/resolve/get_shortest_path.c			\
	src/resolve/waiting_list.c			\
	src/resolve/move_ants.c				\
	src/resolve/resolve.c				\
	src/resolve/free_path.c				\
	src/resolve/check.c

OBJ	= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -Iincludes -g

LDFLAGS	= -lncursesw

all:		$(NAME)

$(NAME)	:	$(OBJ)
		$(CC) $(OBJ) -o $(NAME) $(LDFLAGS)
		@echo -e "[032m$(NAME) compiled successfully[0m"

clean:
		rm -f $(OBJ)

fclean:		clean
		rm -f $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
